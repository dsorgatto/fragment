package br.ufms.facom.fragment;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import br.ufms.facom.MainActivity;
import br.ufms.facom.R;
import br.ufms.facom.adater.CustomListAdapter;
import br.ufms.facom.app.AppController;
import br.ufms.facom.model.Movie;

public class MovieFragment extends Fragment{
	private static final String ARG_SECTION_NUMBER = "section_number";
	private static final String url = "http://www.facom.ufms.br/~marcio/movies.json";
	private ProgressDialog pDialog;
	private List<Movie> movieList = new ArrayList<Movie>();
	private ListView listView;
	private CustomListAdapter adapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		listView = (ListView) getView();
		adapter = new CustomListAdapter(getActivity().getParent(), movieList);
		listView.setAdapter(adapter);
		
		pDialog = new ProgressDialog(getActivity());
		// mostrando uma progressbar
		pDialog.setMessage("Carregando...");
		pDialog.show();
		
		// Criando um objeto Volley
		JsonArrayRequest movieReq = new JsonArrayRequest(url, new Response.Listener<JSONArray>()
		{
			@Override
			public void onResponse(JSONArray response)
			{
				hidePDialog();
				
				// Parseando o JSON
				for (int i = 0; i < response.length(); i++)
				{
					try
					{
						
						JSONObject obj = response.getJSONObject(i);
						Movie movie = new Movie();
						movie.setTitle(obj.getString("title"));
						movie.setThumbnailUrl(obj.getString("image"));
						movie.setRating(((Number) obj.get("rating")).doubleValue());
						movie.setYear(obj.getInt("releaseYear"));
						
						// Os gêneros dos filmes estão em um Array
						JSONArray genreArry = obj.getJSONArray("genre");
						ArrayList<String> genre = new ArrayList<String>();
						for (int j = 0; j < genreArry.length(); j++)
						{
							genre.add((String) genreArry.get(j));
						}
						movie.setGenre(genre);
						
						// Adicionar o filme no Array de Filmes
						movieList.add(movie);
						
					}
					catch (JSONException e)
					{
						e.printStackTrace();
					}
					
				}
				
				// Noficando o array adapter que os dados mudaram
				// assim ele exibirá a lista com os dados atualizados
				adapter.notifyDataSetChanged();
			}
		}, new Response.ErrorListener()
		{
			@Override
			public void onErrorResponse(VolleyError error)
			{
				hidePDialog();
				
			}
		});
		
		// Adicionando uma requisição à Fila
		AppController.getInstance().addToRequestQueue(movieReq);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_main, container, false);
		return rootView;
	}
	
	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		((MainActivity) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		hidePDialog();
	}
	
	private void hidePDialog()
	{
		if (pDialog != null)
		{
			pDialog.dismiss();
			pDialog = null;
		}
	}
}
